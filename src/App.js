import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import AddNew from './components/AddNew';
import Confirmation from './components/Confirmation';
import Register from './components/Register';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={AddNew} />
          <Route path="/register" component={Register} />
          <Route path="/confirmation" component={Confirmation} />
        </Switch>
      </div>
    );
  }
}

export default App;
