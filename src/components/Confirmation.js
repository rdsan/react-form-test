import React, { Component } from 'react';

class Confirmation extends Component {
  constructor(props) {
    super(props);
    if (props.location.state) {
      this.state = props.location.state;
    }
  }

  state = {}

  fakeFetch = (url, obj) => new Promise((resolve, reject) => resolve({ status: 200 }));

  generateOutputImage = (ev) => {
    const file = ev.target.files[0];
    const reader = new FileReader();
    const self = this;

    // Only process image files.
    if (!file.type.match('image*')) {
      return;
    }

    reader.onload = function(fileData) {
      self.setState({
        imageSrc: fileData.target.result,
      });
    };

    // Read in the image file as a data URL
    reader.readAsDataURL(file);

    self.setState({ image: file });
  }

  handleSubmit = (ev) => {
    const pushTo = this.props.history.push;
    ev.preventDefault();

    this.fakeFetch('/', {
      body: JSON.stringify(this.state),
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      mode: 'cors',
      redirect: 'follow',
      referrer: 'no-referrer',
    }).then(function (response) {
      if (response.status === 200) {
        pushTo('/');
      }
    });
  }

  componentDidMount() {
    const { state } = this.props.location;
    if (state) {
      this.setState({
        ...state,
        image: '',
      });
    } else {
      this.props.history.push('/')
    }
  }

  goBackToRegister = () => {
    const { state } = this;
    this.props.history.push({
      pathname: '/register',
      state,
    })
  }

  render() {
    const {
      imageSrc,
      name,
      lastName,
      age,
      email,
      phone,
      state,
      interestsList,
      receiveNews,
    } = this.state;
    const interestsFormatted = interestsList.join(', ').replace(/,([^,]*)$/, ' e$1');

    return (
      <div className="register">
        <form post="" onSubmit={this.handleSubmit}>
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-4 end-xs">
                <label className="field-photo">
                  {imageSrc && (
                    <output>
                      <img src={imageSrc} height={80} width={80} alt="" />
                    </output>
                  )}
                </label>

                <ul>
                  <li>
                    <label>
                      <input
                        style={{display: 'none'}}
                        type="file"
                        name="image"
                        onChange={this.generateOutputImage}
                      />
                      <span>Editar sua foto</span>
                    </label>
                  </li>
                  <li>
                    <button onClick={this.goBackToRegister}>Editar Perfil</button>
                  </li>
                </ul>

              </div>
              <div className="col-xs-12 col-sm-8">
                {`Eu sou o ${name} ${lastName} e eu tenho mais de ${age} anos. `}
                {email && `Você pode enviar e-mails para ${email}. `}
                {`Eu moro no estado do ${state}. `}
                {interestsFormatted && `Eu gosto de ${interestsFormatted}. `}
                {receiveNews && `Por favor, envie-me newsletters. `}
                {phone && `Para me contatar ligue no telefone ${phone}. `}

                <button type="submit">Confirmar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default Confirmation;
