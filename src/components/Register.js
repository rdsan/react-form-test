import React, { Component } from 'react';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = props.location.state || this.state;
  }

  state = {
    image: '',
    imageSrc: '',
    name: '',
    lastName: '',
    age: '13',
    email: '',
    phone: '',
    state: 'Paraná',
    country: 'Brasil',
    addressType: '',
    addressHome: '',
    addressCompany: '',
    interests: '',
    interestsList: [],
    receiveNews: false,
  }

  removeInterest = (idx) => {
    this.setState((prevState) => {
      const interestsList = prevState.interestsList.filter((item, index) => idx !== index);
      const interests = interestsList.join(', ');
      return {
        ...prevState,
        interestsList,
        interests,
      }
    });
  }

  generateOutputImage = (ev) => {
    const file = ev.target.files[0];
    const reader = new FileReader();
    const self = this;

    // Only process image files.
    if (!file.type.match('image*')) {
      return;
    }

    reader.onload = function(fileData) {
      self.setState({
        imageSrc: fileData.target.result,
      });
    };

    // Read in the image file as a data URL
    reader.readAsDataURL(file);

    self.setState({ image: file });
  }

  handleSubmit = (ev) => {
    const { state } = this;
    ev.preventDefault();
    this.props.history.push({
      pathname: '/confirmation',
      state,
    })
  }

  handleChange = (ev) => {
    const target = ev.target;
    const state = target.name;
    let value = target.type === "checkbox" ? target.checked : target.value;

    this.setState((prevState) => {
      let interestsList = prevState.interestsList;

      if (state === 'interests') {
        interestsList = value.split(/, |,/);
        value = interestsList.join(', ');
      }

      return {
        ...prevState,
        [state]: value,
        interestsList,
      }
    });
  }

  render() {
    const {
      // image,
      imageSrc,
      name,
      lastName,
      age,
      email,
      phone,
      state,
      country,
      addressType,
      addressHome,
      addressCompany,
      interests,
      interestsList,
      receiveNews,
    } = this.state;

    const ageOptions = {
      '13': '13-19',
      '20': '20-29',
      '30': '30-45',
      '45': '45 e acima',
    }

    return (
      <div className="register">
        <form onSubmit={this.handleSubmit}>
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-4 end-xs">
                <label className="field-photo">
                  <input
                    className="form-field"
                    type="file"
                    name="image"
                    onChange={this.generateOutputImage}
                  />
                  {imageSrc ? (
                    <output>
                      <img src={imageSrc} height={80} width={80} alt="" />
                    </output>
                  ) : (
                    <span>Carregue sua foto</span>
                  )}
                </label>
              </div>
              <div className="col-xs-12 col-sm-8">
                <label className="form-control">
                  <span className="form-label">Nome</span>
                  <span className="container">
                    <span className="row">
                      <span className="col-xs-12 col-sm-4">
                        <input
                          className="form-field"
                          type="text"
                          name="name"
                          value={name}
                          pattern="[a-zA-Z ]+"
                          onChange={this.handleChange}
                          placeholder="Primeiro Nome"
                          maxLength="20"
                          required
                        />
                      </span>
                      <span className="col-xs-12 col-sm-4">
                        <input
                          className="form-field"
                          type="text"
                          name="lastName"
                          value={lastName}
                          onChange={this.handleChange}
                          placeholder="Sobrenome"
                          required
                        />
                      </span>
                    </span>
                  </span>
                </label>
                <label className="form-control">
                  <span className="form-label">Idade</span>
                  <span className="field-age">
                    <input
                      className="form-field"
                      type="range"
                      list="age-options"
                      min="13"
                      max="45"
                      step="1"
                      name="age"
                      value={age}
                      onChange={this.handleChange}
                      required
                    />
                    <datalist id="age-options">
                      {ageOptions && Object.keys(ageOptions).map((key, idx) => (
                        <option key={`age-options=${key}`} value={key} label={ageOptions[key]} />
                      ))}
                    </datalist>
                  </span>
                </label>
                <label className="form-control">
                  <span className="form-label">E-mail</span>
                  <input
                    className="form-field"
                    type="email"
                    placeholder="david@example.com"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                    required
                  />
                </label>
                <label className="form-control">
                  <span className="form-label">Telefone</span>
                  <input
                    className="form-field"
                    type="tel"
                    placeholder="(41) 99999-9999"
                    name="phone"
                    value={phone}
                    onChange={this.handleChange}
                    required
                  />
                </label>
                <label className="form-control">
                  <span className="form-label">Estado</span>
                  <select
                    className="form-field"
                    name="state"
                    value={state}
                    onChange={this.handleChange}
                    required
                  >
                    <option>Paraná</option>
                    <option>Rio de Janeiro</option>
                    <option>São Paulo</option>
                  </select>
                </label>
                <label className="form-control">
                  <span className="form-label">País</span>
                  <select
                    className="form-field"
                    name="country"
                    value={country}
                    onChange={this.handleChange}
                    required
                  >
                    <option>Brasil</option>
                  </select>
                </label>
                <label className="form-control">
                  <span className="form-label">Endereço</span>
                  <select
                    className="form-field"
                    name="addressType"
                    value={addressType}
                    onChange={this.handleChange}
                    required
                  >
                    <option value="casa">Casa</option>
                    <option value="empresa">Empresa</option>
                  </select>
                </label>
                <div className="form-control form-control--no-label">
                  {addressType === 'casa' ? (
                    <input
                      className="form-field"
                      type="text"
                      name="addressHome"
                      value={addressHome}
                      onChange={this.handleChange}
                      placeholder="R. Francisco Rocha, 1367 - Bigorrilho"
                      required
                    />
                  ) : (
                    <input
                      className="form-field"
                      type="text"
                      name="addressCompany"
                      value={addressCompany}
                      onChange={this.handleChange}
                      placeholder="R. Saldanha Marinho, 3456 - Campina do Siqueira"
                      required
                    />
                  )}
                </div>
                <div className="form-control">
                  <label className="form-label" htmlFor="interests">Interesse</label>
                  <input
                    className="form-field"
                    type="text"
                    id="interests"
                    name="interests"
                    value={interests}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-control form-control--no-label">
                  <div className="interests-list">
                    {interestsList && interestsList.map((interest, idx) => (
                      <div key={`interest-item-${idx}`} className="interests-item">
                        {interest && (
                          <div>
                            {interest.trim()} <button type="button" onClick={() => this.removeInterest(idx)}>X</button>
                          </div>
                        )}
                      </div>
                    ))}
                  </div>
                </div>
                <label className="form-control form-control--no-label">
                  <input
                    type="checkbox"
                    name="receiveNews"
                    checked={receiveNews}
                    onChange={this.handleChange}
                  />
                  Desejo receber novidade por e-mail
                </label>
                <div className="form-control--no-label">
                  <button type="submit">Salvar</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default Register;
