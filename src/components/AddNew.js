import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AddNew extends Component {
  render() {
    return (
      <div className="add-new">
        <Link to="/register">Cadastro</Link>
      </div>
    );
  }
}

export default AddNew;
